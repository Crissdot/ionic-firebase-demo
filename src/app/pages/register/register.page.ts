import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  email: string;
  password:string;
  name: string;
  confirmPassword: string;

  constructor(
    private authService: AuthService,
    private alert: AlertService
    ) { }

  ngOnInit() {}

  createUser(){
    if(!this.name || !this.email || !this.password || !this.confirmPassword) return this.alert.errorAlert('Debes llenar todos los campos');
    if(this.password !== this.confirmPassword) return this.alert.errorAlert('Las contraseñas no coinciden');
    this.authService.createUserEmail(this.email, this.password, this.name).catch(() => this.alert.errorAlert('Email inválido'));
  }
}
