import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { RoomService } from '../../services/room.service';
import { AlertService } from '../../services/alert.service';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { PhotosService } from '../../services/photos.service';

const { Camera } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  rooms: any = [];
  photo: SafeResourceUrl;
  albumPhotos = [];
  uploading = false;
  percent: number;

  constructor(
    private authService: AuthService,
    private roomService: RoomService,
    private alert: AlertService,
    private sanitizer: DomSanitizer,
    public photosService: PhotosService
    ) {}

  ngOnInit(){
    this.showRooms();
    this.showPhotos();
  }

  showPhotos(){
    this.photosService.readImages().subscribe(images => {
      this.albumPhotos = images;
    });
  }

  async takePicture(){
    // Take a photo
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.DataUrl, 
      source: CameraSource.Camera, 
      quality: 100,
      allowEditing: false
    }).then(image => {
      this.uploading = true;
      this.photosService.saveImage(image.dataUrl);
      this.photosService.uploadPercent.subscribe(percent => {
        this.percent = percent;
        if(percent === 100){
          setTimeout(() => {
            this.uploading = false;
          }, 1500);
        }
      })
      this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image.dataUrl);
    })
  }

  deleteImg(image){
    this.photosService.deleteImages(image);
  }

  logout(){
    this.authService.logout();
  }

  showRooms(){
    this.roomService.readRooms().subscribe(rooms => {
      this.rooms = rooms;
    })
  }

  addRoom(){
    this.alert.addRoomAlert();
  }

  updateRoom(room){
    this.alert.updateRoomAlert(room.id, room.name);
  }

  deleteRoom(room){
    this.roomService.deleteRooms(room.id);
  }
}
