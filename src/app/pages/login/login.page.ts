import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  password: string; 

  constructor(
    private authService: AuthService,
    private alert: AlertService
    ) { }

  ngOnInit() {}

  loginEmail(){
    if(!this.email || !this.password) return this.alert.errorAlert('Debes llenar todos los campos');
    this.authService.loginUserEmail(this.email, this.password).catch(() => this.alert.errorAlert('Email o contraseña incorrecto'));
  }

}
