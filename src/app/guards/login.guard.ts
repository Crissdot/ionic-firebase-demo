import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { ToastService } from '../services/toast.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
    private toast: ToastService
    ) { }

  canActivate() {
    if(this.authService.isAuthenticated()) return true;
    this.toast.showToast('Debes iniciar sesión');
    this.router.navigate(['/register']);
    return false;
  }
  
}
