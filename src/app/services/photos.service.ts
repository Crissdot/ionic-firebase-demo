import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastService } from './toast.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  uploadPercent: Observable<number>;

  constructor(
    private storage: AngularFireStorage,
    private database: AngularFirestore,
    private toast: ToastService
  ) { }

  dbRef = this.database.collection('images');
  storageRef = this.storage.ref('images/');
  
  saveImage(image){
    const namefile = `${new Date().getTime()}.jpg`;
    const task =this.storageRef.child(`${namefile}`).putString(image, 'data_url');
    this.uploadPercent = task.percentageChanges();

    task.then(img => {
      img.ref.getDownloadURL().then(url => {
        this.createImage(namefile, url)
          .then(() => this.toast.showToast('Imagen almacenada correctamente'));
      });
    }).catch(console.log);
  }

  removeImage(name){
    this.storageRef.child(`${name}`).delete();
  }

  createImage(nameFile, path){
    return this.dbRef.add({
      name: nameFile,
      url: path
    });
  }

  readImages(){
    return this.dbRef.snapshotChanges().pipe(map(images => {
      return images.map(image => {
        const data: Object = image.payload.doc.data();
        const id = image.payload.doc.id;
        return {id, ...data};
      })
    }))
  }

  deleteImages(image){
    this.dbRef.doc(image.id).delete().then(() => {
      this.removeImage(image.name);
      this.toast.showToast('Imagen eliminada correctamente');
    }).catch(console.log);
  }

}
