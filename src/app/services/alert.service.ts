import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { RoomService } from './room.service';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    private room: RoomService,
    private alertController: AlertController
  ) { }

  async addRoomAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Add New Room',
      inputs: [
        {
          name: 'nameRoom',
          type: 'text',
          placeholder: 'Name room'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (name) => {
            this.room.createRoom(name.nameRoom);
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

  async updateRoomAlert(id: string, name: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Update Room',
      inputs: [
        {
          name: 'nameRoom',
          type: 'text',
          value: name
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (name) => {
            this.room.updateRooms(id, name.nameRoom);
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

  async errorAlert(message: string){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alerta',
      message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }
}
