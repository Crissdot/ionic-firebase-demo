import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastService } from './toast.service';
import { User } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: User;

  constructor(
    private Auth: AngularFireAuth,
    private router: Router,
    private Database: AngularFirestore,
    private toast: ToastService
    ) { 
      this.Auth.authState.subscribe(user => {
        if (user){
          this.user = user;
          localStorage.setItem('user', JSON.stringify(this.user.email));
        } else {
          localStorage.setItem('user', null);
        }
      })
    }

  isAuthenticated(){
    return this.isLoggedIn;
  }

  loginUserEmail(email: string, password: string){
    return this.Auth.signInWithEmailAndPassword(email,password).then(()=> {
      setTimeout(() => {
        this.toast.showToast('Has iniciado sesión');
        this.router.navigate(['/home']);
      }, 100)
    });
  }

  createUserEmail(email: string, password: string, name: string){
    return this.Auth.createUserWithEmailAndPassword(email, password).then(user => {
      const uid = user.user.uid;
      return this.Database.collection('users').doc(uid).set({
        uid,
        name,
        email
      }).then(() => {
        this.toast.showToast('Usuario creado correctamente');
        this.router.navigate(['/home']);
      });
    });
  }

  logout(){
    return this.Auth.signOut().then(() => {
      localStorage.removeItem('user');
      this.toast.showToast('Esperamos verte pronto');
      this.router.navigate(['/login']);
    }).catch(console.log);
  }

  get isLoggedIn(): boolean {
    const  user  =  JSON.parse(localStorage.getItem('user'));
    return  user  !==  null;
  }
}
