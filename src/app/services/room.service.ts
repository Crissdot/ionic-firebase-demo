import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(
    private Database: AngularFirestore,
    
    private toast: ToastService
    ) { }

  DBRef = this.Database.collection('rooms');

  createRoom(name: string){
    this.DBRef.add({
      name,
      status: true
    }).then(() => {
      this.toast.showToast('Elemento creado correctamente');
    }).catch(console.log);
  }

  readRooms(){
    return this.DBRef.snapshotChanges().pipe(map(rooms => {
      return rooms.map(room => {
        const data: Object = room.payload.doc.data();
        const id = room.payload.doc.id;
        return {id, ...data};
      })
    }))
  }

  updateRooms(id: string, name:string){
    this.DBRef.doc(id).update({
      name
    }).then(() => {
      this.toast.showToast('Elemento actualizado correctamente');
    }).catch(console.log);
  }

  deleteRooms(id: string){
    this.DBRef.doc(id).delete().then(() => {
      this.toast.showToast('Elemento eliminado correctamente')
    }).catch(console.log);
  }
}
